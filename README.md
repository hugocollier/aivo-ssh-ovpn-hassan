```
Source: https://github.com/Diegoprofeta/SSHPLUS-8
```
## Installation
```
apt update && wget https://gitlab.com/hugocollier/aivo-ssh-ovpn-hassan/-/raw/main/install.sh; chmod 777 install.sh; ./install.sh
```

## How to use websocket Non-TLS
* Please use applications like HI and HC then set your SNI as Hostname of SSH, set port 80 then fill in user/pass, next use the payload below 

## How to use websocket TLS
* Please use HC and set as usual, fill in hostname on server address and SNI on SNI then set port 443, fill in user/pass then use payload below


```bash
websocket payload

GET / HTTP/1.1[crlf]Host: www.youtube.com[crlf]Upgrade: websocket[crlf][crlf]
```

